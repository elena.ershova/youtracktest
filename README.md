# youtrackTest
Youtrack UI tests, build 5.2.5-8823 based on Java + Selenide + Junit


## Getting started

You have to:
- instal java version 8 or later
- download maven (https://maven.apache.org/download.cgi) and add it to environmental variables
- start local apache server
- install youtrack build:
```
java -Djetbrains.youtrack.baseUrl=http://localhost:8080 -Duser.home=./ -jar youtrack-5.2.5-8823.jar 8080
```

## Run tests

```
cd <folder containing pom.xml>
mvn install test
`
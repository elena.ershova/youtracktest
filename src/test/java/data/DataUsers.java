package data;

public enum DataUsers {
    ROOT("root", "YoutrackTest"),
    GUEST("guest", "");

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    private String login;
    private String password;

    private DataUsers(String login, String password) {
        this.login = login;
        this.password = password;
    }

}

package helpers;

import components.AdminRegisterForm;
import components.LoginForm;
import pages.UsersPage;

import java.util.logging.Logger;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.url;
import static org.junit.Assert.assertEquals;

public class Navigator {
    private static final Logger LOGGER = Logger.getLogger("Navigator logger");
    private static final String BASE_URL = "http://localhost:8080";
    private static final String DASHBOARD = "/dashboard";
    private static final String USERS = "/users";
    private static final String LOGIN = "/login";

    public static void openBaseUrl(){
        open(BASE_URL);
        assertEquals("Unexpected url has been opened!", BASE_URL + DASHBOARD, url());
    }

    public static LoginForm goToLogin() {
        LOGGER.info("Go to " + BASE_URL + LOGIN);
        open(BASE_URL + LOGIN);
        return new LoginForm();
    }

    public static AdminRegisterForm goToAdminRegistration() {
        return goToUsersPage().clickCreateUser();
    }

    public static UsersPage goToUsersPage() {
        LOGGER.info("Go to " + BASE_URL + USERS);
        open(BASE_URL + USERS);
        return new UsersPage();
    }
}

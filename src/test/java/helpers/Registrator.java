package helpers;

public class Registrator {

    public static void createUserMandatoryFields(String login, String password) {
        Navigator
                .goToAdminRegistration()
                .fillMandatory(login, password, password)
                .clickOk()
                .successState()
                .goToUsers();
    }
}

package pages;

import base.BaseComponent;
import components.FooterComponent;
import components.HeaderNavigationComponent;

import java.util.logging.Logger;

public abstract class YoutrackPage extends BaseComponent {
    protected static final Logger LOGGER = Logger.getLogger("UI components logger");

    protected static HeaderNavigationComponent headerNavigationComponent;
    protected static FooterComponent footerComponent;

    public YoutrackPage() {
        headerNavigationComponent = new HeaderNavigationComponent();
        footerComponent = new FooterComponent();
    }

    @Override
    protected void checkComponent() {
        check();
    }

    protected abstract void check();
}

package pages;

import com.codeborne.selenide.Condition;
import components.FooterComponent;
import components.HeaderNavigationComponent;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class DashboardPage extends YoutrackPage {
    private static HeaderNavigationComponent headerNavigationComponent;
    private static FooterComponent footerComponent;
    private static final By FILTER_SEARCH = By.name("l.D.sb.filterFolder");
    private static final By TEXT_SEARCH = By.name("l.D.sb.searchField");
    private static final By SEARCHES = By.xpath(".//*[@tabid='Searches']");
    private static final By HINTS = By.xpath(".//*[@tabid='Hints']");
    private static final By SAVED_SEARCHES = By.id("id_l.D.wc.ws.watchFolders");

    @Override
    protected void check() {
        $(FILTER_SEARCH).shouldBe(Condition.visible.because("Filter search not found"));
        $(TEXT_SEARCH).shouldBe(Condition.visible.because("Text search input not found"));
        $(SEARCHES).shouldBe(Condition.visible.because("Searches button not found"));
        $(HINTS).shouldBe(Condition.visible.because("Hints link not found"));
        $(SAVED_SEARCHES).shouldBe(Condition.visible.because("Saved search block not found"));
        LOGGER.info("Dashboard page has been loaded");
    }
}

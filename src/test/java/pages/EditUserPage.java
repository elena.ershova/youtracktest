package pages;

import com.codeborne.selenide.Condition;
import components.AdminMenu;
import components.FooterComponent;
import components.HeaderNavigationComponent;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class EditUserPage extends YoutrackPage {
    private static HeaderNavigationComponent headerNavigationComponent;
    private static FooterComponent footerComponent;
    private static AdminMenu adminMenu;
    private static final By BREADCRUMB = By.id("id_l.E.AdminBreadcrumb.AdminBreadcrumb");
    private static final By GROUP = By.xpath(".//*[@tabid = 'User Group']");
    private static final By ROLES = By.xpath(".//*[@tabid = 'User Own Roles']");
    private static final By PERMISSIONS = By.xpath(".//*[@tabid = 'Permissions View']");
    private static final By TAB_PANEL = By.xpath(".//*[@class = 'jt-tabpanel-content']");

    public EditUserPage() {
        adminMenu = new AdminMenu();
    }

    @Override
    protected void check() {
        $(BREADCRUMB).shouldBe(Condition.visible.because("No breadcrumbs link header"));
        $(GROUP).shouldBe(Condition.visible.because("No group tab"));
        $(ROLES).shouldBe(Condition.visible.because("No roles tab"));
        $(PERMISSIONS).shouldBe(Condition.visible.because("No permissions tab"));
        $(TAB_PANEL).shouldBe(Condition.visible.because("No tab panel"));
        LOGGER.info("Edit User page has been loaded");
    }

    public UsersPage goToUsers() {
        return adminMenu.clickUsersLink();
    }
}

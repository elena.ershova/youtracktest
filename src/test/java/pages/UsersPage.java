package pages;

import com.codeborne.selenide.Condition;
import components.*;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class UsersPage extends YoutrackPage {
    private static AdminMenu adminMenu;
    private static UsersTableComponent usersTableComponent;
    private static AdminSearchComponent adminSearchComponent;
    private static final By USER_PANEL = By.id("id_l.U.userPanel");
    private static final By NOTE = By.xpath(".//p[@class='note']");
    private static final By CREATE_USER = By.id("id_l.U.createNewUser");

    public UsersPage() {
        adminMenu = new AdminMenu();
        adminSearchComponent = new AdminSearchComponent();
        usersTableComponent = new UsersTableComponent();
    }

    @Override
    public void check() {
        $(USER_PANEL).shouldBe(Condition.visible.because("No main block: user panel"));
        $(NOTE).shouldBe(Condition.visible.because("No registration note"));
        $(CREATE_USER).shouldBe(Condition.visible.because("No create user link"));
        LOGGER.info("Users page has been loaded");
    }

    public UsersTableComponent getUsersTableComponent() {
        return usersTableComponent;
    }

    public AdminRegisterForm clickCreateUser() {
        LOGGER.info("Click link to create new user");
        $(CREATE_USER).click();
        return new AdminRegisterForm();
    }
}

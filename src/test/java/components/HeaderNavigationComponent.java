package components;

import base.BaseComponent;
import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class HeaderNavigationComponent extends BaseComponent {
    private static final String HEADER = "[id *= '.header']";
    private static final String LOGO_BTN = "[href = '/dashboard']";
    private static final String ISSUES_LINK = "[href = '/issues']";
    private static final String ISSUES_DROPDOWN = ".yt-header__issues-toggler";
    private static final String AGILE_BOARDS = "[href = '/rest/agile']";
    private static final String REPORTS = "[href = '/rest/reports']";
    private static final String USER = ".yt-header__user-toggle";
    private static final String HELP = ".ring-font-icon_help";
    private static final String CREATE_ISSUE = "[href = '#newissue=yes']";
    private static final String SETTINGS = "[data-ring-dropdown *= 'Projects']";
    private static final By USERS = By.xpath(".//*[@class='ring-dropdown__item ring-link' and @href = '/users']");
    private static final String LOGIN_LINK = "[class *= 'yt-header__login-link']";
    private static final String REGISTER_LINK = "[class *= 'yt-header__signin-link']";

    @Override
    protected void checkComponent() {
        $(HEADER).shouldBe(Condition.exist.because("No main header panel element"));
        $(HEADER).$(LOGO_BTN).shouldBe(Condition.visible.because("No logo button"));
        $(HEADER).$(ISSUES_LINK).shouldBe(Condition.visible.because("No issues link"));
        $(HEADER).$(ISSUES_DROPDOWN).shouldBe(Condition.visible.because("No issues dropdown"));
        $(HEADER).$(AGILE_BOARDS).shouldBe(Condition.visible.because("No agile boards link"));
        $(HEADER).$(REPORTS).shouldBe(Condition.visible.because("No reports link"));
        $(HEADER).$(USER).shouldBe(Condition.visible.because("No user link"));
        $(HEADER).$(HELP).shouldBe(Condition.visible.because("No help link"));
    }
}

package components;

import base.BaseComponent;
import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import pages.UsersPage;

import static com.codeborne.selenide.Selenide.$;

public class AdminMenu extends BaseComponent {
    private static final By MENU = By.id("id_l.AdminMenu.adminMenu");
    private static final By PROJECTS = By.xpath(".//*[@href = '/projects']");
    private static final By USERS = By.xpath(".//*[@href = '/users']");
    private static final By GROUPS = By.xpath(".//*[@href = '/groups']");
    private static final By ROLES = By.xpath(".//*[@href = '/roles']");
    private static final By ISSUE_LINK_TYPES = By.xpath(".//*[@href = '/issueLinkTypes']");
    private static final By SETTINGS = By.xpath(".//*[@href = '/settings']");
    private static final By DATABASE_BACKUP = By.xpath(".//*[@href = '/databaseBackup']");
    private static final By TEXT_INDEX = By.xpath(".//*[@href = '/textIndex']");
    private static final By STATISTICS = By.xpath(".//*[@href = '/statistics']");
    private static final By JIRA_INTEGRATION = By.xpath(".//*[@href = '/jiraIntegration']");
    private static final By CUSTOM_FIELDS = By.xpath(".//*[@href = '/customFieldsConfiguration']");
    private static final By LDAP = By.xpath(".//*[@href = '/ldapAuthenticationSettings']");
    private static final By MAILBOX = By.xpath(".//*[@href = '/mailBox']");
    private static final By NOTIFICATION_TEMPLATES = By.xpath(".//*[@href = '/notificationTemplates']");
    private static final By OPEN_ID = By.xpath(".//*[@href = '/openIdIntegrationSettings']");
    private static final By SSL_MANAGEMENT = By.xpath(".//*[@href = '/sslManagement']");
    private static final By TEAMCITY = By.xpath(".//*[@href = '/teamcityIntegrationSettings']");
    private static final By TIME_TRACKING = By.xpath(".//*[@href = '/workSchedule']");
    private static final By WORKFLOW = By.xpath(".//*[@href = '/workflows']");
    private static final By ZENDESK = By.xpath(".//*[@href = '/zendeskIntegrationSettings']");

    @Override
    protected void checkComponent() {
        $(MENU).shouldBe(Condition.visible.because("Admin menu not found"));
        $(MENU).$(PROJECTS).shouldBe(Condition.visible.because("No project link found"));
        $(MENU).$(USERS).shouldBe(Condition.visible.because("No users link found"));
        $(MENU).$(GROUPS).shouldBe(Condition.visible.because("No groups link found"));
        $(MENU).$(ROLES).shouldBe(Condition.visible.because("No roles link found"));
        $(MENU).$(ISSUE_LINK_TYPES).shouldBe(Condition.visible.because("No issue link types link found"));
        $(MENU).$(SETTINGS).shouldBe(Condition.visible.because("No settings link found"));
        $(MENU).$(DATABASE_BACKUP).shouldBe(Condition.visible.because("No database link found"));
        $(MENU).$(TEXT_INDEX).shouldBe(Condition.visible.because("No text index link found"));
        $(MENU).$(STATISTICS).shouldBe(Condition.visible.because("No statistics link found"));
        $(MENU).$(JIRA_INTEGRATION).shouldBe(Condition.visible.because("No jira link found"));
        $(MENU).$(CUSTOM_FIELDS).shouldBe(Condition.visible.because("No custom fields link found"));
        $(MENU).$(LDAP).shouldBe(Condition.visible.because("No LDAP link found"));
        $(MENU).$(NOTIFICATION_TEMPLATES).shouldBe(Condition.visible.because("No notification templates link found"));
        $(MENU).$(MAILBOX).shouldBe(Condition.visible.because("No mailbox link found"));
        $(MENU).$(OPEN_ID).shouldBe(Condition.visible.because("No open id link found"));
        $(MENU).$(SSL_MANAGEMENT).shouldBe(Condition.visible.because("No ssl management link found"));
        $(MENU).$(TEAMCITY).shouldBe(Condition.visible.because("No teamcity link found"));
        $(MENU).$(TIME_TRACKING).shouldBe(Condition.visible.because("No time tracking link found"));
        $(MENU).$(WORKFLOW).shouldBe(Condition.visible.because("No workflow link found"));
        $(MENU).$(ZENDESK).shouldBe(Condition.visible.because("No zendesk link found"));
    }

    public UsersPage clickUsersLink() {
        $(USERS).click();
        return new UsersPage();
    }
}

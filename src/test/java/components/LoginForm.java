package components;

import base.BaseComponent;
import com.codeborne.selenide.Condition;
import data.DataUsers;
import org.openqa.selenium.By;
import pages.DashboardPage;


import static com.codeborne.selenide.Selenide.$;

public class LoginForm extends BaseComponent {

    private static final String LOGO = ".logo";
    private static final By LOGIN_GUEST = By.id("id_l.L.loginAsGuestLink");
    private static final By LOGIN = By.id("id_l.L.login");
    private static final By PASSWORD = By.id("id_l.L.password");
    private static final By LOGIN_BTN = By.id("id_l.L.loginButton");
    private static final By REMEMBER = By.id("id_l.L.rememberMeLabel");

    @Override
    protected void checkComponent() {
        $(LOGO).shouldBe(Condition.visible.because("No logo button"));
        $(LOGIN_GUEST).shouldBe(Condition.visible.because("No login as guest link"));
        $(LOGIN).shouldBe(Condition.visible.because("No login input"));
        $(PASSWORD).shouldBe(Condition.visible.because("No password input"));
        $(REMEMBER).shouldBe(Condition.exist.because("No remember me checkbox"));
        $(LOGIN_BTN).shouldBe(Condition.visible.because("No login buton"));
        LOGGER.info("Login form has been opened");
    }

    public void login(String username, String password) {
        LOGGER.info("Logging in as " + username + "/" + password);
        $(LOGIN).sendKeys(username);
        $(PASSWORD).sendKeys(password);
        $(LOGIN_BTN).click();
    }

    public DashboardPage login(DataUsers user) {
        LOGGER.info("Logging in as " + user.getLogin() + "/" + user.getPassword());
        if(user == DataUsers.GUEST) {
            return loginAsGuest();
        }
        $(LOGIN).sendKeys(user.getLogin());
        $(PASSWORD).sendKeys(user.getPassword());
        $(LOGIN_BTN).click();
        return new DashboardPage();
    }

    public DashboardPage loginAsGuest() {
        $(LOGIN_GUEST).click();
        return new DashboardPage();
    }
}

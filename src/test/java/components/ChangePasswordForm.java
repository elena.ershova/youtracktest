package components;

import base.BaseComponent;
import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import static com.codeborne.selenide.Selenide.$;

public class ChangePasswordForm extends BaseComponent {
    private static final By POPUP = By.id("id_l.U.ChangePasswordDialog.changePasswordDlg");
    private static final By OLD_PASSWORD = By.id("id_l.U.ChangePasswordDialog.oldPassword");
    private static final By NEW_PASSWORD = By.id("id_l.U.ChangePasswordDialog.newPassword1");
    private static final By CONFIRM_NEW_PASSWORD = By.id("id_l.U.ChangePasswordDialog.newPassword2");
    private static final By SAVE = By.id("id_l.U.ChangePasswordDialog.passOk");

    @Override
    protected void checkComponent() {
        $(POPUP).shouldBe(Condition.visible.because("No popup present"));
        $(OLD_PASSWORD).shouldBe(Condition.visible.because("No old password input"));
        $(NEW_PASSWORD).shouldBe(Condition.visible.because("No new password input"));
        $(CONFIRM_NEW_PASSWORD).shouldBe(Condition.visible.because("No new password confirm input"));
        $(SAVE).shouldBe(Condition.visible.because("No save button"));
    }
}

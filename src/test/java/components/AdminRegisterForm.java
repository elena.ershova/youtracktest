package components;

import base.BaseComponent;
import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import pages.UsersPage;
import promises.RegisterPromise;

import static com.codeborne.selenide.Selenide.$;

public class AdminRegisterForm extends BaseComponent {
    private static final By POPUP = By.id("id_l.U.cr.main");
    private static final By LOGIN = By.id("id_l.U.cr.login");
    private static final By PASSWORD = By.id("id_l.U.cr.password");
    private static final By CONFIRM_PASSWORD = By.id("id_l.U.cr.confirmPassword");
    private static final By FORCE_CHANGE_PASSWORD = By.id("id_l.U.cr.forcePasswordChange");
    private static final By FULLNAME = By.id("id_l.U.cr.fullName");
    private static final By EMAIL = By.id("id_l.U.cr.email");
    private static final By JABBER = By.id("id_l.U.cr.jabber");
    private static final By OK_BUTTON = By.id("id_l.U.cr.createUserOk");
    private static final By CANCEL_BUTTON = By.id("id_l.U.cr.createUserCancel");
    private static final By ERROR = By.xpath(".//*[@class = 'error-bulb2']");
    private static final By ERROR_MSG = By.xpath(".//*[@class = 'message error']");
    private static final By ERROR_MSG_TEXT = By.xpath(".//*[@class = 'errorSeverity']");

    @Override
    protected void checkComponent() {
        $(POPUP).shouldBe(Condition.visible.because("No registration popup"));
        $(LOGIN).shouldBe(Condition.visible.because("No login input"));
        $(PASSWORD).shouldBe(Condition.visible.because("No password input"));
        $(CONFIRM_PASSWORD).shouldBe(Condition.visible.because("No confirm password input"));
        $(FORCE_CHANGE_PASSWORD).shouldBe(Condition.visible.because("No force change password checkbox"));
        $(FULLNAME).shouldBe(Condition.visible.because("No fullname input"));
        $(EMAIL).shouldBe(Condition.visible.because("No email input"));
        $(JABBER).shouldBe(Condition.visible.because("No jabber input"));
        $(OK_BUTTON).shouldBe(Condition.visible.because("No OK button"));
        $(CANCEL_BUTTON).shouldBe(Condition.visible.because("No Cancel button"));
    }

    public AdminRegisterForm fillMandatory(String login, String password, String confirmPassword) {
        fillLogin(login);
        fillPassword(password);
        fillConfirmPassword(confirmPassword);
        return this;
    }

    public AdminRegisterForm fillLogin(String login) {
        $(LOGIN).sendKeys(login);
        return this;
    }

    public AdminRegisterForm fillPassword(String password) {
        $(PASSWORD).sendKeys(password);
        return this;
    }

    public AdminRegisterForm fillConfirmPassword(String password) {
        $(CONFIRM_PASSWORD).sendKeys(password);
        return this;
    }

    public AdminRegisterForm fillFullname(String name) {
        $(FULLNAME).sendKeys(name);
        return this;
    }

    public AdminRegisterForm fillEmail(String email) {
        $(EMAIL).sendKeys(email);
        return this;
    }

    public AdminRegisterForm fillJabber(String jabber) {
        $(JABBER).sendKeys(jabber);
        return this;
    }

    public AdminRegisterForm enableChangePassword() {
        $(FORCE_CHANGE_PASSWORD).click();
        return this;
    }

    public UsersPage clickCancel() {
        $(CANCEL_BUTTON).click();
        return new UsersPage();
    }

    public RegisterPromise clickOk() {
        $(OK_BUTTON).click();
        return new RegisterPromise();
    }

    public boolean isErrorPresent() {
        return $(ERROR).isDisplayed();
    }

    public boolean isErrorMessagePresent() {
        return $(ERROR_MSG).isDisplayed();
    }

    public String getErrorMessage() {
        return $(ERROR_MSG).$(ERROR_MSG_TEXT).getText();
    }
}

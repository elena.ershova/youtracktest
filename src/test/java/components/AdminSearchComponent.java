package components;

import base.BaseComponent;
import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class AdminSearchComponent extends BaseComponent {
    private static final By SEARCH_PANEL = By.xpath(".//*[@class = 'jt-panel jt-fieldset']");
    private static final By FIND_USER = By.id("id_l.U.queryText");
    private static final By SHOW_ONLINE_CHECKBOX = By.id("id_l.U.onlineOnlyLabel");
    private static final By GROUP = By.name("l.U.groupFilter");
    private static final By ROLE = By.name("l.U.roleFilter");
    private static final By PROJECT = By.name("l.U.projectFilter");
    private static final By PERMISSION = By.name("l.U.permissionFilter");
    private static final By SEARCH_BUTTON = By.id("id_l.U.searchButton");
    private static final By RESET_BUTTON = By.id("l.U.resetButton");

    @Override
    protected void checkComponent() {
        $(SEARCH_PANEL).shouldBe(Condition.visible.because("No search panel"));
        $(FIND_USER).shouldBe(Condition.visible.because("No search input"));
        $(SHOW_ONLINE_CHECKBOX).shouldBe(Condition.visible.because("No online checkbox"));
        $(GROUP).shouldBe(Condition.visible.because("No group filter"));
        $(ROLE).shouldBe(Condition.visible.because("No role filter"));
        $(PROJECT).shouldBe(Condition.visible.because("No project filter"));
        $(PERMISSION).shouldBe(Condition.visible.because("No permission filter"));
        $(SEARCH_BUTTON).shouldBe(Condition.visible.because("No search button"));
    }
}

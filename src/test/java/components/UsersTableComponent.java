package components;

import base.BaseComponent;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import wrappers.UserWrapper;

import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class UsersTableComponent extends BaseComponent {
    private static final By USERS_TABLE = By.id("id_l.U.usersList.usersList");
    private static final By LOGIN = By.xpath(".//th[text() = 'Login ']");
    private static final By FULL_NAME = By.xpath(".//th[text() = 'Full name']");
    private static final By EMAIL = By.xpath(".//th[text() = 'Email/Jabber']");
    private static final By GROUPS = By.xpath(".//th[text() = 'Groups']");
    private static final By LAST_ACCESS = By.xpath(".//th[text() = 'Last access']");
    private static final By EDIT_USER_LINK = By.xpath(".//*[contains(@id, 'id_l.U.usersList.UserLogin.editUser_')]");
    private static final By TBODY = By.xpath(".//tbody");
    private static final By TR = By.xpath(".//tr");
    private static final By TD = By.xpath(".//td");
    private static final By DELETE = By.xpath(".//*[@cn = 'l.U.usersList.deleteUser']");

    @Override
    protected void checkComponent() {
        $(USERS_TABLE).shouldBe(Condition.visible.because("No table block"));
        $(USERS_TABLE).$(LOGIN).shouldBe(Condition.visible.because("No login column header"));
        $(USERS_TABLE).$(FULL_NAME).shouldBe(Condition.visible.because("No full name column header"));
        $(USERS_TABLE).$(EMAIL).shouldBe(Condition.visible.because("No email column header"));
        $(USERS_TABLE).$(GROUPS).shouldBe(Condition.visible.because("No groups column header"));
        $(USERS_TABLE).$(LAST_ACCESS).shouldBe(Condition.visible.because("No last access column header"));
        $(USERS_TABLE).$(EDIT_USER_LINK).shouldBe(Condition.visible.because("No edit user link"));
    }

    public static List<UserWrapper> getUsers() {
        return $(USERS_TABLE).$(TBODY).$$(TR)
                .stream()
                .map(UsersTableComponent::wrap)
                .collect(Collectors.toList());
    }

    public static UserWrapper getUser(String login) {
        return getUsers()
                .stream()
                .filter(userWrapper -> userWrapper.getLogin().equals(login))
                .findFirst()
                .orElseThrow(() -> new AssertionError("Cannot find user: " + login));
    }

    public static boolean isUserPresent(String login) {
        return getUsers()
                .stream()
                .anyMatch(user -> user.getLogin().equals(login));
    }

    public static void deleteAllUsers() {
        while (getUsers().size() >2 && $(DELETE).isDisplayed()) {
            $(DELETE).click();
            switchTo().alert().accept();
            refresh();
        }
        assertEquals(2, getUsers().size(), "Should be 2 users only: guest & root");
        LOGGER.info("All users have been deleted");
    }

    private static UserWrapper wrap(SelenideElement row) {
        ElementsCollection tds = row.$$(TD);
        return new UserWrapper(tds.get(0).getText(),
                tds.get(1).getText(),
                tds.get(2).getText(),
                tds.get(3).getText());
    }
}

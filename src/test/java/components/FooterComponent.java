package components;

import base.BaseComponent;
import com.codeborne.selenide.Condition;

import static com.codeborne.selenide.Selenide.$;

public class FooterComponent extends BaseComponent {
    private static final String FOOTER = ".ring-footer";

    @Override
    protected void checkComponent() {
        $(FOOTER).shouldBe(Condition.visible.because("No footer!"));
    }
}

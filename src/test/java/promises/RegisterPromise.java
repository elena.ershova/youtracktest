package promises;

import components.AdminRegisterForm;
import pages.EditUserPage;

public class RegisterPromise {
    public AdminRegisterForm errorState() {
        return new AdminRegisterForm();
    }

    public EditUserPage successState() {
        return new EditUserPage();
    }
}

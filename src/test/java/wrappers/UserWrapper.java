package wrappers;

public class UserWrapper {
    private String login;
    private String fullname;
    private String email;
    private String groups;


    public UserWrapper(String login, String fullname, String email, String groups) {
        this.login = login;
        this.fullname = fullname;
        this.email = email;
        this.groups = groups;
    }

    public String getLogin() {
        return login;
    }

    public String getFullname() {
        return fullname;
    }

    public String getEmailJabber() {
        return email;
    }

    public String getGroups() {
        return groups;
    }
}

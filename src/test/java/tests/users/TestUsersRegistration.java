package tests.users;

import base.BaseTest;
import components.AdminRegisterForm;
import components.ChangePasswordForm;
import components.UsersTableComponent;
import data.DataUsers;
import helpers.Navigator;
import helpers.Registrator;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.jupiter.api.Test;
import wrappers.UserWrapper;

import static org.junit.jupiter.api.Assertions.*;

public class TestUsersRegistration extends BaseTest {
    private static final String PASSWORD = RandomStringUtils.randomAlphanumeric(15);
    private static final String EMAIL = "qwerty@mail.ru";
    private static final String JABBER = "qwerty@jabber.ru";

    @Test
    public void testCreateWithMandatoryData() {
        LOGGER.info("Create new user with mandatory data only");
        String username = RandomStringUtils.randomAlphabetic(10);

        Navigator.goToLogin().login(DataUsers.ROOT);
        Registrator.createUserMandatoryFields(username, PASSWORD);
        assertTrue(UsersTableComponent.isUserPresent(username), "Not found new user.");
    }

    @Test
    public void testCreateWithAdditionalData() {
        LOGGER.info("Create new user with all data.");
        String username = RandomStringUtils.randomAlphabetic(10);
        String fullname = username + " Surname" + RandomStringUtils.randomAlphabetic(3);

        Navigator.goToLogin().login(DataUsers.ROOT);
        Navigator.goToUsersPage()
                .clickCreateUser()
                .fillMandatory(username, PASSWORD, PASSWORD)
                .fillFullname(fullname)
                .fillEmail(EMAIL)
                .fillJabber(JABBER)
                .clickOk()
                .successState()
                .goToUsers();
        assertTrue(UsersTableComponent.isUserPresent(username), "Not found new user.");
        UserWrapper user = UsersTableComponent.getUser(username);
        assertAll("Check user fields",
                () -> assertEquals(fullname, user.getFullname(), "Wrong fullname"),
                () -> assertTrue(user.getEmailJabber().contains(EMAIL), "No email found"),
                () -> assertTrue(user.getEmailJabber().contains(JABBER), "No jabber found"));
    }

    @Test
    public void testCancelRegistration() {
        LOGGER.info("Cancel registering new user.");
        String username = RandomStringUtils.randomAlphabetic(10);

        Navigator.goToLogin().login(DataUsers.ROOT);
        Navigator.goToUsersPage()
                .clickCreateUser()
                .fillMandatory(username, PASSWORD, PASSWORD)
                .clickCancel();
        assertFalse(UsersTableComponent.isUserPresent(username), "User should not be found");
    }

    @Test
    public void testForcePasswordChange() {
        LOGGER.info("Create new user with force change password option.");
        String username = RandomStringUtils.randomAlphabetic(10);

        Navigator.goToLogin().login(DataUsers.ROOT);
        Navigator.goToUsersPage()
                .clickCreateUser()
                .fillMandatory(username, PASSWORD, PASSWORD)
                .enableChangePassword()
                .clickOk()
                .successState()
                .goToUsers();
        assertTrue(UsersTableComponent.isUserPresent(username), "Not found new user.");
        Navigator.goToLogin().login(username, PASSWORD);
        new ChangePasswordForm();
    }

    @Test
    public void testErrorAndContinue() {
        LOGGER.info("Create user with error and continue with correct data");
        String username = RandomStringUtils.randomAlphabetic(10);

        Navigator.goToLogin().login(DataUsers.ROOT);
        AdminRegisterForm registerForm = Navigator
                .goToUsersPage()
                .clickCreateUser()
                .fillMandatory("", PASSWORD, PASSWORD)
                .clickOk()
                .errorState();
        assertTrue(registerForm.isErrorPresent(), "No error present");
        registerForm.fillLogin(username)
                .clickOk()
                .successState()
                .goToUsers();
        assertTrue(UsersTableComponent.isUserPresent(username), "Not found new user.");
    }
}

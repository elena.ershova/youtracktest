package tests.users;

import base.BaseTest;
import components.AdminRegisterForm;
import components.UsersTableComponent;
import data.DataUsers;
import helpers.Navigator;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestUsersRegistrationNegative extends BaseTest {
    private static final String PASSWORD = RandomStringUtils.randomAlphanumeric(15);
    private static final String ERROR_MSG_LOGIN = "Restricted character";

    @Test
    public void testNoLogin() {
        LOGGER.info("Check error with no login");
        Navigator.goToLogin().login(DataUsers.ROOT);
        AdminRegisterForm registerForm = Navigator
                .goToUsersPage()
                .clickCreateUser()
                .fillMandatory("", PASSWORD, PASSWORD)
                .clickOk()
                .errorState();
        assertTrue(registerForm.isErrorPresent(), "No error present under login input");
    }

    @Test
    public void testWrongLogin() {
        LOGGER.info("Check error with wrong login");
        String username = " qwerty";

        Navigator.goToLogin().login(DataUsers.ROOT);
        AdminRegisterForm registerForm = Navigator
                .goToUsersPage()
                .clickCreateUser()
                .fillMandatory(username, PASSWORD, PASSWORD)
                .clickOk()
                .errorState();
        assertTrue(registerForm.isErrorMessagePresent(), "No error message present above");
        assertTrue(registerForm.getErrorMessage().contains(ERROR_MSG_LOGIN), "Wrong error message");
    }

    @Test
    public void testXSSLogin() {
        LOGGER.info("Check injection username");
        String username = "alert(1)";

        Navigator.goToLogin().login(DataUsers.ROOT);
        Navigator
                .goToUsersPage()
                .clickCreateUser()
                .fillMandatory(username, PASSWORD, PASSWORD)
                .clickOk()
                .successState()
                .goToUsers();
        assertTrue(UsersTableComponent.isUserPresent(username));
    }

    @Test
    public void testTooLongLogin() {
        LOGGER.info("Check too long username");
        String username = RandomStringUtils.randomAlphabetic(100);

        Navigator.goToLogin().login(DataUsers.ROOT);
        Navigator
                .goToUsersPage()
                .clickCreateUser()
                .fillMandatory(username, PASSWORD, PASSWORD)
                .clickOk()
                .successState()
                .goToUsers();
        assertTrue(UsersTableComponent.isUserPresent(username.substring(0, 50)), "Username should be trimmed to 50 characters");
    }

    @Test
    public void testNoPassword() {
        LOGGER.info("Check error with no password");
        String username = RandomStringUtils.randomAlphabetic(10);

        Navigator.goToLogin().login(DataUsers.ROOT);
        AdminRegisterForm registerForm = Navigator
                .goToUsersPage()
                .clickCreateUser()
                .fillMandatory(username, "", "")
                .clickOk()
                .errorState();
        assertTrue(registerForm.isErrorPresent(), "No error present under password input");
    }

    @Test
    public void testWrongPassword() {
        LOGGER.info("Check error with wrong password");
        String username = RandomStringUtils.randomAlphabetic(10);
        String password = "   ";

        Navigator.goToLogin().login(DataUsers.ROOT);
        AdminRegisterForm registerForm = Navigator
                .goToUsersPage()
                .clickCreateUser()
                .fillMandatory(username, password, password)
                .clickOk()
                .errorState();
        assertTrue(registerForm.isErrorMessagePresent(), "No error message above about unsupported characters in password");
    }

    @Test
    public void testPasswordConfirmationFail() {
        LOGGER.info("Check error with password confirmation");
        String username = RandomStringUtils.randomAlphabetic(10);

        Navigator.goToLogin().login(DataUsers.ROOT);
        AdminRegisterForm registerForm = Navigator
                .goToUsersPage()
                .clickCreateUser()
                .fillMandatory(username, PASSWORD, "qwerty")
                .clickOk()
                .errorState();
        assertTrue(registerForm.isErrorPresent(), "No error under 2nd password input present");
    }

    @Test
    public void testTooLongFullname() {
        LOGGER.info("Check too long username");
        String username = RandomStringUtils.randomAlphabetic(10);
        String fullname = RandomStringUtils.randomAlphabetic(200);

        Navigator.goToLogin().login(DataUsers.ROOT);
        Navigator
                .goToUsersPage()
                .clickCreateUser()
                .fillMandatory(username, PASSWORD, PASSWORD)
                .fillFullname(fullname)
                .clickOk()
                .successState()
                .goToUsers();
        assertTrue(UsersTableComponent.isUserPresent(username), "User not found");
        assertEquals(fullname.substring(0, 50), UsersTableComponent.getUser(username).getFullname(), "Fullname should be trimmed to 50 characters");
    }

    @Test
    public void testWrongEmail() {
        LOGGER.info("Check wrong email");
        String username = RandomStringUtils.randomAlphabetic(10);
        String email = "usermailru";

        Navigator.goToLogin().login(DataUsers.ROOT);
        AdminRegisterForm registerForm = Navigator
                .goToUsersPage()
                .clickCreateUser()
                .fillMandatory(username, PASSWORD, PASSWORD)
                .fillEmail(email)
                .clickOk()
                .errorState();
        assertTrue(registerForm.isErrorPresent(), "No error under email: wrong format");
    }

    @Test
    public void testTooLongEmail() {
        LOGGER.info("Check too long email");
        String username = RandomStringUtils.randomAlphabetic(10);
        String email = "qwerty@" + RandomStringUtils.randomAlphabetic(300) + ".ru";

        Navigator.goToLogin().login(DataUsers.ROOT);
        AdminRegisterForm registerForm = Navigator
                .goToUsersPage()
                .clickCreateUser()
                .fillMandatory(username, PASSWORD, PASSWORD)
                .fillEmail(email)
                .clickOk()
                .errorState();
        assertTrue(registerForm.isErrorPresent(), "No error under email: too long");
    }

    @Test
    public void testTooLongJabber() {
        LOGGER.info("Check too long jabber");
        String username = RandomStringUtils.randomAlphabetic(10);
        String jabber = "jabber@" + RandomStringUtils.randomAlphabetic(300) + ".ru";

        Navigator.goToLogin().login(DataUsers.ROOT);
        AdminRegisterForm registerForm = Navigator
                .goToUsersPage()
                .clickCreateUser()
                .fillMandatory(username, PASSWORD, PASSWORD)
                .fillJabber(jabber)
                .clickOk()
                .errorState();
        assertTrue(registerForm.isErrorPresent(), "No error under jabber: too long");
    }

}

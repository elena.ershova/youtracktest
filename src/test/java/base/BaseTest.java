package base;
import data.DataUsers;
import helpers.Navigator;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;

import static com.codeborne.selenide.Selenide.open;

import java.util.logging.Logger;

public class BaseTest {
    protected static final Logger LOGGER = Logger.getLogger("Test logger");

    @BeforeEach
    public void init() {
        Navigator.openBaseUrl();
    }

    @AfterAll
    public static void deleteCustomUsers() {
        Navigator.goToLogin().login(DataUsers.ROOT);
        Navigator.goToUsersPage().getUsersTableComponent().deleteAllUsers();
    }
}


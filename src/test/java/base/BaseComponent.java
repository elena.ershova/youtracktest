package base;

import java.util.logging.Logger;

public abstract class BaseComponent {
    protected static final Logger LOGGER = Logger.getLogger("UI components logger");

    public BaseComponent() {
        checkComponent();
    }

    protected abstract void checkComponent();
}
